import { Lang } from "./js/langs.js";
import { setLang } from "./js/setLang.js";
import { delLang } from "./js/deleteLang.js";
import { addLang } from "./js/addLang.js";

$(document).ready(function () {
  $("#table_id").DataTable({
    language: {
      url: "./French.json",
    },
  });
});

let lang = new Lang();

addLang(name);
setLang(lang);
delLang(lang);
