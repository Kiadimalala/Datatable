import { counter, addCounter } from "./setLang.js";
import { LANG } from "../js/mock-language.js";
import { setLang } from "../js/setLang.js";

export function addLang() {
  $("#submit").click(function () {
    let name = $("input").val();
    let item = { id: counter, name };

    if (name === "") {
      let _err = $(".form");
      let _errTemplate =
        '<p class="err text-red-500 text-center m-2">Veuillez saisir un nom</p>';
      if ($(".err").length === 0) {
        _err.append(_errTemplate);

        //Remove err message after delay
        setTimeout(function () {
          $(".err").remove();
        }, 2000);
      }
    } else {
      
      $(document).ready(function(){
         let t = $("#table_id").DataTable();
        let del = '<button id="delete" class="delete bg-gray-200 ml-2 p-1 focus:outline-none text-xs m-2">Supprimer</button>'
        t.row
          .add([
            item.id,
            item.name,del
          ])
          .draw(false);
      })
    
      LANG.push(item);
      let LANG_JSON = JSON.stringify(LANG);
      localStorage.setItem("LANG", LANG_JSON);
  
      $("input").val(null);
      addCounter();
    }
  });
}
