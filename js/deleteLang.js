import { counter, delCounter, setLang } from "./setLang.js";

export function delLang() {
  $(document).on("click", ".delete", function () {
    $(document).ready(function () {
      let t = $("#table_id").DataTable();
      t.row($(this).parent()[0]).remove().draw();
    });

    let id = $(this).parent().siblings().html();
    let langItem = JSON.parse(localStorage["LANG"]);
    langItem = langItem.filter((item) => item.id != id);
    let langJson = JSON.stringify(langItem);
    localStorage.setItem("LANG", langJson);

    delCounter();

  });
}
